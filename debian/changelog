xwallpaper (0.7.3-1) unstable; urgency=medium

  * New upstream release
  * Bump Debian Standards Version to 4.6.0

 -- Sebastian Reichel <sre@debian.org>  Sat, 18 Sep 2021 20:26:02 +0200

xwallpaper (0.6.5-1) unstable; urgency=medium

  * New upstream release
  * Bump Debian Standards Version to 4.5.1
  * Switch to compat level 13
  * Update debian/watch format to level 4

 -- Sebastian Reichel <sre@debian.org>  Mon, 04 Jan 2021 10:39:29 +0100

xwallpaper (0.6.3-1) unstable; urgency=medium

  * New upstream release
  * Bump Debian Standards Version to 4.5.0

 -- Sebastian Reichel <sre@debian.org>  Sat, 14 Mar 2020 00:22:06 +0100

xwallpaper (0.6.2-2) unstable; urgency=medium

  * Bump Debian Standards Version to 4.4.1
  * Lower libjpeg-dev dependency (Closes: #940456)
  * Switch to compat level 12

 -- Sebastian Reichel <sre@debian.org>  Sat, 04 Jan 2020 10:35:23 +0100

xwallpaper (0.6.2-1) unstable; urgency=medium

  * New upstream release
  * Bump Debian Standards Version to 4.4.0

 -- Sebastian Reichel <sre@debian.org>  Fri, 05 Jul 2019 18:42:47 +0200

xwallpaper (0.4.1-1) unstable; urgency=medium

  * New upstream release
   - Adds daemon support to redraw the wallpaper on screen dimension change
  * Depend on libjpeg-dev instead of libjpeg62-turbo-dev (Closes: #910520).
    Do not drop version requirement, since #860219 is not fixed in stable.
  * Bump Debian Standards Version to 4.2.1

 -- Sebastian Reichel <sre@debian.org>  Tue, 18 Dec 2018 00:17:23 +0100

xwallpaper (0.3.0-2) unstable; urgency=medium

  * Switch downstream git repository to salsa
  * Update Debian Standards Version to 4.1.4
  * Switch to compat level 11

 -- Sebastian Reichel <sre@debian.org>  Wed, 06 Jun 2018 16:07:43 +0200

xwallpaper (0.3.0-1) unstable; urgency=medium

  * New upstream release
   - Drop zsh completion patch (applied upstream)
   - Drop libjpeg version check removal (fixed in libjpeg-dev)
  * Added seccomp dependency on Linux
  * Update Debian Standards Version to 4.1.1

 -- Sebastian Reichel <sre@debian.org>  Sun, 15 Oct 2017 16:19:06 +0200

xwallpaper (0.2.1-1) unstable; urgency=low

  * New upstream release
   - Drop patch to decrease minimal xcb version (applied upstream)
  * Mention Debian Bug #860219 in do-not-check-libjpeg-version.patch
  * Cherry-Pick upstream patch adding zsh completion

 -- Sebastian Reichel <sre@debian.org>  Fri, 14 Apr 2017 20:25:24 +0200

xwallpaper (0.2.0-1) unstable; urgency=low

  * Initial release. (Closes: #860283)

 -- Sebastian Reichel <sre@debian.org>  Wed, 12 Apr 2017 21:03:04 +0200
